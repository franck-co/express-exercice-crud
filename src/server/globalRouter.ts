import { Router } from 'express';

import { exampleCtrl } from 'controllers';

const router = Router();


//friendly homepage
router.get('/', function (req, res) {
    const { name, version, description } = require('../../package.json')
    if (req.accepts('html')) res.render('index', { mode: process.env.NODE_ENV, name, version, description });
    else res.json({ "message": `Bienvenue sur l'api ${name}`, version: version || "?", description })
});


router.use('/example', exampleCtrl);
//router.use('/endpoint2', controller2);


export const globalRouter = router