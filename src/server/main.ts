//import 'reflect-metadata';
import express, { NextFunction, Request, Response } from 'express';
import cors from 'cors'
import path from 'path'
import morgan from 'morgan'
import cookieParser from 'cookie-parser'
import createError from 'http-errors'
import {consoleStyles} from 'utils/consoleStyling'
import {globalRouter} from "./globalRouter";



//Set global __basedir to src (entry point is dist/server/main.ts so __dirname is dist/server )
declare global{
  let __basedir:string
}
eval("global.__basedir = require('path').join(__dirname,'..','..','src')")


process.env.NODE_ENV = process.env.NODE_ENV || 'development'

const app = express();
const port = process.env.PORT || 3000;



async function preStart(){
  //include async code that need to be executed before server starts to listen to requests
}


preStart().then(() => {


    //Logger
    app.use(morgan('dev'))

    //request parsing
    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));
    app.use(cookieParser());

    
    //Template engine middlewares
    app.set('views', path.join(__basedir, 'views'));
    app.set('view engine', 'hbs');

    //To serve static files directly (css and images for views)
    app.use(express.static(path.join(__basedir, 'views', 'public')));


    //Security - Network
    app.use(cors({origin:'*'}));

    //Authentication
    //app.use(passport...)


    //dispatch to controllers
    app.use(globalRouter)

    // if no route is matched, create 404 error and forward to error handler
    app.use((req, res, next) => {next(createError(404))});


    // error handler
    app.use(function(err:any, req:Request, res:Response, next:NextFunction) {

      // set locals, only providing error in development
      res.locals.message = err.message;
      res.locals.error = req.app.get('env') === 'development' ? err : {};
      
      res.status(err.status || 500);

      // render the error page or send json
      if(req.accepts('html')) res.render('error');
      else res.json(err)
      
    });

    app.listen(port, () => {
      console.log(consoleStyles.FgGreen,`[ ready ] Startup finished ! Express server started on port ${port} in mode ${process.env.NODE_ENV}`);
    
    });
  })


//export objects that need to be initialized at server start before use
export { /*mikro ,agenda*/ }