import { Router } from 'express';
import { getConnection } from 'utils/mysql';


const router = Router();


router.get('/user/:id', async (req, res) => {
  try {

    const id = parseInt(req.params.id)
    if (isNaN(id)) {
      return res.status(404).json({ message: `id '${req.params.id}' is not a valid number` });
    }

    res.json({ "provided id": id });
  } catch (err: any) {
    return res.status(400).json({ message: err.message });
  }
});

router.post('/', async (req, res) => {
  if (!req.body.expectedData) {
    res.status(400);
    return res.json({ message: 'One of `expectedData` is missing' });
  }


  try {
    const example = Date().toString()
    if (!example) {
      return res.status(404).json({ message: 'Error message' });
    }

    res.json({ "What time is it ?": example });
  } catch (err: any) {
    return res.status(400).json({ message: err.message });
  }
});

type Color = {
  id: number,
  color: string
}

router.get('/colors', async (req, res, next) => {
  try {
    const conn = await getConnection()
    const [queryResult] = await conn.execute(`SELECT * FROM colors`)

    const colors: Array<Color> = queryResult as Array<Color>
    res.json(colors)
  } catch (err: any) {
    next(err)
  }
})

export const exampleCtrl = router;