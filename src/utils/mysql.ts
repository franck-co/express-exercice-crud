import mysql from 'mysql2/promise'




export async function getConnection(){

    const connection = await mysql.createConnection({
        host: 'ip or domain',
        port: 1234,
        user: 'username',
        password:'password',
        database: 'db name',
      });


    return connection
}

