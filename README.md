## __Installation__ ##
```
yarn install
```
<br>

## __Keeping packages up to date__ ##
```
yarn upgrade-interactive --latest
```
Press `space` to select, `a` to toggle all, `i` to inverse selection

<br>

## __Absolute paths__ ##

### *for import statements* ###

Thanks to tsconfig-paths, a npm package that is required before the app is started, via the `-r` flag
 ( `node -r ./tsconfig-paths-boot.js dist/server/main.js`),
 you can use absolute imports.

 ```ts
 //do
 import { exampleCtrl } from 'controllers';
 // insted of
 import { exampleCtrl } from '../../controllers';
 ```


Aliases can also be defined in tsconfig.json

<br>

### *for fs.readFile* ###

A global variable `__basedir` is defined in `server/main.ts` and refers to `src` folder.
That means you can use the same syntax whereever the calling file is located

```ts
//do
fs.readFileSync(path.resolve(__basedir, "ressources", "template.pdf"))
//instead of
fs.readFileSync(path.resolve(__dirname, "..", "..", "ressources", "template.pdf"))
```

Note : As it is initialised in the `server/main.ts` file, using  `__basedir` at the top level of a file required first when the program starts will fail. 

<br>

## __Environment variables__ ##


| Variable name| Example value | Description |
|     :---     |    :---       |     :---    |
| PORT | 3000 (default) | listening port of node server |
| NODE_ENV | development OR staging OR production | used in various places in code to select a comportment like db connection, disabling token authentication in dev mode, or choosing target url of third -party services.|

<br>

env variables can be read from `process.env.MY_VARIABLE`

Add an .env file or specify them in command line.

<br>

## __Running in dev mode__ ##
Build + Start in watch mode
```
yarn start:dev
```

<br>

## __Debugging__

Debbuging is configured for vscode in `.vscode`  folder

2 launch scripts are avaiable and can be launched **directly** form debug section of vscode (or by pressing f5)


|Name|Description|
|     :---     |    :---      |
|debug:build:start|buiid and then start|
|debug:watch| build by tsc-watch and run |

<br>

## __Launch for prod__ ##
Build + Start in watch mode
```
yarn build
yarn start
```

Not all files are compiled and sent to dist folder (template views, pdf documents, json files ...)
Those static files will remain in the src folder. Thus, both src and dist folders are needed for running the server.