FROM node:lts-alpine

ENV PORT=3000
ENV NODE_ENV=production 

WORKDIR /app
COPY . .

RUN yarn install 
RUN yarn run build

EXPOSE $PORT
#CMD ["node", "-r", "/app/tsconfig-paths-boot.js", "/app/dist/server/main.js"]
RUN yarn start
